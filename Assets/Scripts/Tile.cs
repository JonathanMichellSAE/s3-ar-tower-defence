﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public enum TileType
    {
        e_start,
        e_end,
        e_corner,
        e_straight
    }
    public TileType type;
    public Vector3 dir1;
    public Vector3 dir2;
    public GameObject next1 = null;
    public GameObject next2 = null;
    public GameObject pathNext = null;
    public GameObject spawnPrefab;
    float width = 0.11f;

    // Start is called before the first frame update
    bool firstCall = true;
    float spawnTimer = 0;
    void Start()
    {
        GetComponent<LineRenderer>().SetVertexCount(3);
    }

    float mydot(Vector3 a, Vector3 b)
    {
        float x = a.x * b.x;
        float y = a.y * b.y;
        float z = a.z * b.z;

        return x+y+z;
    }
    // Update is called once per frame

    bool canConnect(Vector3 dir, GameObject other)
    {
        Vector3 otherDir1 = other.transform.rotation * other.GetComponent<Tile>().dir1;
        Vector3 otherDir2 = other.transform.rotation * other.GetComponent<Tile>().dir2;
        if(Vector3.Dot(dir,otherDir1)<-0.98f || Vector3.Dot(dir, otherDir2) < -0.98f)
        {
            return true;
        }
        return false;
    }

    GameObject FindNearest(Vector3 dir)
    {
        GameObject[] gs = GameObject.FindGameObjectsWithTag("Tile");
        GameObject next = null;
        float distanceNext = 1000000.0f;
        foreach (GameObject g in gs)
        {
            if (!GameObject.ReferenceEquals(g, gameObject))
            {
                Vector3 forward = transform.rotation * dir;
                Vector3 p = transform.position;
                Vector3 delta = g.transform.position - transform.position;
                float distance = Vector3.Dot(delta, forward);

                Vector3 perp = new Vector3(forward.z, 0, -forward.x);
                float a = Vector3.Dot(delta, perp);
                float b = mydot(delta, perp);
                float side = Vector3.Dot(delta, perp);
                if (distance > 0 && distance < 3.0f && Mathf.Abs(side) < width && canConnect(forward, g)) // Check if it's within our scan area
                {
                    if (distance < distanceNext) // This new one is closer than the last found one
                    {
                        distanceNext = distance;
                        next = g;
                    }
                }
                Debug.DrawRay(p, width * perp, new Color(1.0f, 0.5f, 0));
                Debug.DrawRay(p, -width * perp, new Color(1.0f, 0.5f, 0));
                Debug.DrawRay(p + width * perp, forward * 10, new Color(1.0f, 0.5f, 0));
                Debug.DrawRay(p - width * perp, forward * 10, new Color(1.0f, 0.5f, 0));
                //Debug.Log(side);
            }
        }
        return next;
    }

    void Update()
    {
        if(firstCall)
        {
            //            firstCall = false;
            next1 = FindNearest(dir1);
            next2 = FindNearest(dir2);

                GetComponent<LineRenderer>().SetVertexCount(3);
                GetComponent<LineRenderer>().SetWidth(0.01f, 0.01f);
                GetComponent<LineRenderer>().SetPosition(1, transform.position);
                if (next1 != null)
                {
                    //                Debug.DrawLine(transform.position, next1.transform.position,Color.green);

                    GetComponent<LineRenderer>().SetPosition(0, next1.transform.position);
                }
                else
                {
                    GetComponent<LineRenderer>().SetPosition(0, transform.position);
                }
                if (next2 != null)
                {
                    GetComponent<LineRenderer>().SetPosition(2, next2.transform.position);
                }
                else
                {
                    GetComponent<LineRenderer>().SetPosition(2, transform.position);
                }
        }


        if(type==TileType.e_start)
        {
            //spawnTimer -= Time.deltaTime;
            //if (spawnTimer <=0)
            //{
            //    spawnTimer = 1.0f;
            //    GameObject g = Instantiate(spawnPrefab, transform.position, transform.rotation);
            //    g.GetComponent<Enemy>().currentTile = gameObject;
            //}

            GameObject[] gs = GameObject.FindGameObjectsWithTag("Tile");
            foreach (GameObject g in gs)
            {
                g.GetComponent<Tile>().pathNext = null;
            }


            List<GameObject> nodes = new List<GameObject>();
 //           nodes.Add(gameObject);
            GameObject current = gameObject;
            GameObject last = null;
            int count = current.transform.childCount;
            Debug.Log(count);
            Waypoints.points.Clear();
            Waypoints.complete = false;
            while (current != null)
            {
                if (nodes.Count > 20)
                {
                    Debug.Log("INFINITE!");
                    break;
                }
                nodes.Add(current);
                if(current.GetComponent<Tile>().type == TileType.e_end)
                {
                    Waypoints.complete = true;
                }
                Waypoints.points.Add(current.transform.Find("Waypoint"));
                GameObject t1 = current.GetComponent<Tile>().next1;
                GameObject t2 = current.GetComponent<Tile>().next2;

                if (t1 != null && t1 != last)
                {
                    last = current;
                    current.GetComponent<Tile>().pathNext = t1;
                    current = t1;
                    continue;
                }
                else
                if (t2 != null && t2 != last)
                {
                    last = current;
                    current.GetComponent<Tile>().pathNext = t2;
                    current = t2;
                    continue;
                }
                break;
            }


            Debug.Log(nodes.Count);
            LineRenderer lr = GetComponent<LineRenderer>();
            lr.SetVertexCount(nodes.Count);
            lr.SetColors(Color.black, Color.green);
            lr.SetWidth(0.07f, 0.01f);
            for (int i = 0; i < nodes.Count; ++i)
            {
                lr.SetPosition(i, nodes[i].transform.position + new Vector3(0, 0.01f, 0));
            }
        }
        //Debug.DrawRay(transform.position, transform.rotation * dir1 *10, Color.red);
        //Debug.DrawRay(transform.position, transform.rotation * dir2 *10, Color.blue);
    }
}
