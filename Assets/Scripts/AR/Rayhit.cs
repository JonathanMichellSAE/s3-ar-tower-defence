using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
public class Rayhit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void HandleRaycast(ARRaycastHit hit)
    {
        // Determine if it is a plane
        if ((hit.hitType & TrackableType.Planes) != 0)
        {
            // Do something with 'plane':
            Debug.Log($"Hit a plane");
        }
        else
        {
            // What type of thing did we hit?
            Debug.Log($"Raycast hit a {hit.hitType}");
        }
    }

}
