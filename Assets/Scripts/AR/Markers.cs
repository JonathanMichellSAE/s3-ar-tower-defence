using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
public class Markers : MonoBehaviour
{
    ARTrackedImageManager m_TrackedImageManager;
    public GameObject prefab;
    public GameObject prefab2;

    public WaveSpawner waveSpawner;

    [Header("Grid Prefabs")]
    public GameObject S_Start;
    public GameObject E_End;
    public GameObject H_Horizontal;
    public GameObject V_Vertical;
    public GameObject C_Corner1;
    public GameObject T_Corner2;

    public Dictionary<string, GameObject> MapDictionary;

    void Awake()
    {
        m_TrackedImageManager = GetComponent<ARTrackedImageManager>();
        MapDictionary = new Dictionary<string, GameObject>
        {
            { "S", S_Start},
            { "E", E_End},
            { "H1", H_Horizontal},
            { "V1", V_Vertical},
            { "C1", C_Corner1},
            { "T1", T_Corner2}
        };
    }

    void OnEnable()
    {
        m_TrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
    }

    void OnDisable()
    {
        m_TrackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
    }

    void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (var trackedImage in eventArgs.added)
        {
            GameObject markerPrefab = MapDictionary[trackedImage.referenceImage.name];
            
            if (markerPrefab != null)
            {
                GameObject tileInstantiated = Instantiate(markerPrefab, trackedImage.transform);
                
                if (trackedImage.referenceImage.name == "S")
                {
                    waveSpawner.spawnPoint = tileInstantiated.transform;
                }
            }

            // Give the initial image a reasonable default scale
            //trackedImage.transform.localScale = new Vector3(0.01f, 1f, 0.01f);
            //if (trackedImage.referenceImage.name == "download")
            //{
            //    Instantiate(prefab, trackedImage.transform);

            //}
            //else
            //{
            //    Instantiate(prefab2, trackedImage.transform);
            //}

        }

        foreach (var trackedImage in eventArgs.updated)
        {
            // Instantiate(prefab2, trackedImage.transform);
        }
    }
}
