using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

	public string levelToLoad = "MainLevel";

	public SceneFader sceneFader;

	public void Play()
	{
		sceneFader.FadeTo(levelToLoad);
	}

	public void Quit()
	{
		// If in the Unity Editor, turns off play mode
#if UNITY_EDITOR
		{
			Debug.Log("Quitting the game...");
			UnityEditor.EditorApplication.isPlaying = false;
		}
		// If in application, exit application
#else
        {
            Application.Quit();
        }
#endif
	}

}
